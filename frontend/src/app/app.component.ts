import { Component, OnInit } from '@angular/core';
import { Observable, timer } from "rxjs";
import { FormControl, Validators } from '@angular/forms';

import { SearchResult } from './types';
import { ApiService } from "./api.service"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {

  query = new FormControl('', []);
  doc = new FormControl('', []);

  fileList: FileList | null = null;

  loading: Boolean = false;

  results: SearchResult[] = [];

  constructor(private api: ApiService) { }

  ngOnInit() { }

  onQuerySubmit() {
    if ( this.query.value == "" ) {
      alert("Please, input a query.");
      return;
    }

    this.loading = true;

    let queryMap: Map<string, number> = new Map();

    let hasKey: boolean = false;
    let key: string = "";
    for (let word of (this.query.value as string).split(/[\s]+/)) {
      if (!hasKey) {
        key = word;
        hasKey = true;
      }
      else if (!isNaN(Number(word.replace(",", ".")))) {
        queryMap.set(key, Number(word.replace(",", ".")));
        hasKey = false;
      } else {
        queryMap.set(key, 1);
        key = word;
      }
    }

    if (hasKey)
      queryMap.set(key, 1);

    let query: Array<{key: string, value: number}> = [];
    for ( let pair of queryMap.entries() )
      query.push( { key: pair[0], value: pair[1] } );

    console.log( "Sending query." );
    this.api.getQuery( JSON.stringify( query ) ).subscribe( (response) => { this.loading = false; this.results = response; } );
  }

  onDocSubmit() {
    if (this.fileList?.length === 0)
      return;

    this.loading = true;
    if (this.fileList as FileList) {
      this.api.sendDoc(this.fileList as FileList)?.subscribe(response => {
        this.loading = false;
        console.log(response);
        if (response !== 1)
          alert("Something went wrong.");
      });
      alert("Document submited");
    } else
      alert("Something went very wrong.");

    this.doc.reset();
    this.fileList = null;

    return;
  }

  onDocChange(event: Event) {
    if ((event.target as HTMLInputElement).files) {
      this.fileList = (event.target as HTMLInputElement).files;
    } else {
      alert("Failiure!");
      return;
    }

    // submit form
    this.onDocSubmit();
  }

  onCompute() {
    this.loading = true;
    this.api.compute().subscribe(response => {
      this.loading = false;
      if (response !== 1)
        alert("Something went wrong.");
    });
  }

}
