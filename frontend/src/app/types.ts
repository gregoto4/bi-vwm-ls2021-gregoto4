export interface SearchResult {
  document: string;
  weight: number;
};

