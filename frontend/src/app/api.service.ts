import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { SearchResult } from './types';
import { Observable } from 'rxjs';

const HOST = window.location.hostname;
const PORT = 8000;

const ADDRESS = "http://" + HOST + ":" + PORT;

const QUERY = "/query";
const THRESHOLD = "/";
const DOC = "/data";
const CALC = "/compute";


@Injectable({
  providedIn: 'root',
})
export class ApiService {

  constructor(private http: HttpClient) {}

  getQuery( query: string ) : Observable<SearchResult[]> {
    let params = new HttpParams().set( "query", query )
    return this.http.get<SearchResult[]>( ADDRESS + QUERY, {params} );
  }

  sendDoc( data: FileList ) : null | Observable<number>  {
    if ( data?.length == undefined || data?.length <= 0 )
      return null;

    const formData = new FormData();
    for ( let i = 0; i < data.length; i++ )
      formData.append('file', data[i], data[i].name);

    return this.http.post<number>( ADDRESS + DOC, formData );

  }

  compute() : Observable<Number> {
    return this.http.get<number>( ADDRESS + CALC );
  }

}