from flask import Flask
import flask
from flask.json import jsonify
import json

import scipy
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import svds
import numpy as np

app = Flask(__name__)

@app.route('/', methods=["POST"])
def calculate(  ):

  arr = json.loads( flask.request.get_json()["data"] );

  A = csc_matrix(arr, dtype=float)

  u, s, vt = svds(A, k = min( min(A.shape) - 1, 30))

  search_matrix = np.diag(s) @ vt
  return jsonify( { "uk": u.tolist() , "dk": search_matrix.tolist() } );
