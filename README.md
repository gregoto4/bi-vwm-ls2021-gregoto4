# Dependencies

- Docker
- npm

# Usage

```
cd frontend
npm install
cd ..
docker-compose up
```

# Documentation

- Frontend - http://localhost:4200
- Backend - http://localhost:8000
- Solver - http://localhost:5000
- Database - postgresql://localhost:5432