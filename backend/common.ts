/*
  Common types
*/
export interface Word {
  id: number;
  word: string;
};

export interface Document {
  id: number;
  name: string;
};

export interface DocumentWord {
  id: number;
  word_id: string;
  count: number;
  weight: number;
};

export interface QueryResult {

};