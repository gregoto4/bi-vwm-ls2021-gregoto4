import { sendWords } from "../services/DbService.ts";
import { Response, FormDataFile } from "../deps.ts";
// import { client, Document } from "../deps.ts";


export default async ( files: FormDataFile[] ) : Promise<number> => {

  const rg = /([a-zA-Z0-9\-]+)/gm;
  const promises: Promise<void>[] = [];

  for ( const f of files ) {

    const file: string = await Deno.readTextFile( f.filename as string );

    const words: string[] = [];
    let m;

    do {
      m = rg.exec( file );
      if ( m ) {
        words.push( ( m[1] as string ).toLowerCase() )
      }
    } while ( m );

    promises.push( sendWords( f.originalName as string, words ) );
  }

  await Promise.all( promises );

  return 1;

};