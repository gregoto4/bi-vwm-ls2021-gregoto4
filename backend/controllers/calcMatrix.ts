// import { sendWords } from "../services/DbService.ts";
import { client } from "../deps.ts";
import ConceptRepo from "../repositories/ConceptRepo.ts";
import UBaseRepo from "../repositories/UBaseRepo.ts";

export default async () => {

  
  let sql = `SELECT (SELECT COUNT(1) from word) as rows, (SELECT COUNT(1) from doc) as columns;`;
  const shape = (await client.queryObject<{rows: number, columns: number}>(sql)).rows[0];

  const outArr: number[][] = [];
  for ( let i = 0; i < shape.rows; i++ ) {
    outArr[i] = [];
    for ( let j = 0; j < shape.columns; j++ )
      outArr[i][j] = 0;
  }
  
  sql = "SELECT id FROM word ORDER BY id;";
  const words = (await client.queryObject<{id: number}>(sql)).rows;
  let wordMap: number[] = [];

  for ( let i = 0; i < words.length; i++ )
    wordMap[words[i].id] = i;

  
  sql = `SELECT DISTINCT
          word_id,
          wordcnt.doccountbyword,
          maxdoc.wordmax
        FROM
          term_vector
        JOIN
          (SELECT
              word_id,
              max(count) as wordmax
          FROM
            term_vector
          JOIN
            doc USING(id)
          GROUP BY
              word_id) as maxdoc USING(word_id)
        JOIN
            (SELECT
              word_id,
              count(word_id) as doccountbyword
          FROM
            term_vector
          GROUP BY
              word_id) as wordcnt USING(word_id)
        ORDER BY
          word_id;`;

const wV = await client.queryObject<{word_id: number, doccountbyword: number, wordmax: number}>(sql);


const wordVals: Map<number, {doccountbyword: number, wordmax: number}> = new Map();
for ( let val of wV.rows)
  wordVals.set( val.word_id, { doccountbyword: val.doccountbyword, wordmax: val.wordmax } );
                        
                        
sql = `SELECT
        id,
        word_id,
        count
      FROM
        term_vector
      ORDER BY
        id, word_id`;

  const qResult = await client.queryObject<{id: number, word_id: number, count: number}>(sql);

  for ( let val of qResult.rows) {
    outArr[wordMap[val.word_id]][val.id - 1] = Number(val.count) / Number( wordVals.get(val.word_id)?.wordmax ) * Math.log2( Number( shape.columns ) / Number( wordVals.get(val.word_id)?.doccountbyword ) );
  }

  const url = 'solver:5000';
  const postRequest = await fetch("http://" + url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ data: JSON.stringify(outArr) }),
  });
  
  console.log( "SVD start." )
  const pResult = await postRequest.json().then(response => JSON.stringify(response));
  console.log("SVD end.")

  const jsonP = JSON.parse(pResult);

  await ConceptRepo.set(jsonP.dk);
  await UBaseRepo.set(jsonP.uk);
  
  return 1;

};
