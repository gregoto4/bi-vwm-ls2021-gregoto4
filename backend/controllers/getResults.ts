import { findDocumnets } from "../services/DbService.ts";

export default async ( params: Record<string, any> ) : Promise<string> => {

  if ( params.query == "" )
    return "";

  console.log( "New query: " + params.query );

  return findDocumnets( params.query );
  
};