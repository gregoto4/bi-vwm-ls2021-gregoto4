import { client } from "../deps.ts";

class WordRepo {
  
  async getMax(wordId: number) : Promise<number> {
    const sql: string = "SELECT MAX(count) AS MAXVAL FROM term_vector WHERE word_id = '" + wordId + "';";
    const max = await client.queryObject(sql);
    return max.rows[0].MAXVAL as number;
  }
  async get (): Promise<string[]> {

    const sql = "SELECT id FROM word;";

    const result = await client.queryArray<[string]>( sql );


    const arr: string[] = result.rows.map( ( w: string[] ) => w[0] )

    return arr;
  }

  async set ( words: Map<string, number> ) {

    if ( words.size <= 0 )
      return;

    let sql = "INSERT INTO word ( word ) VALUES ";

    for ( const w of words.keys() )
      sql += "( '" + w + "' ), ";

    sql = sql.substr( 0, sql.length - 2 );

    sql += " ON CONFLICT (word) DO NOTHING";

    return await client.queryObject( sql );
  }
}

export default new WordRepo();