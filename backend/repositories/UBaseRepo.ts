import { client, Matrix } from "../deps.ts";
// import { FloatMatrix, Matrix, MatrixBuilder } from "../math/index.ts";

class UBaseRepo {
  async deleteTableContent(): Promise<void> {
    const sql = "DELETE FROM ubase_vector; DELETE FROM ubase;";
    await client.queryObject(sql);
    return;
  }
  async set(mat: number[][]): Promise<void> {

    this.deleteTableContent();
    
    let sql: string = "INSERT INTO ubase (id) VALUES ";
    for ( let i = 0; i < mat.length; i++ ) {
      sql += "( " + (i + 1) + " ), ";
    }

    sql = sql.substring(0, sql.length - 2);
    sql += ";";

    await client.queryObject( sql );

    const sqlBase = "INSERT INTO ubase_vector ( id, concept_id, val ) VALUES ";
    sql = sqlBase;

    const promises: Promise<any>[] = [];
    let counter = 0;
    for ( let i = 0; i < mat.length; i++ ) {
      for ( let j = 0; j < mat[0].length; j++ ) {
        sql += "( "+ (i + 1) + ", " + (j + 1) + ", " + mat[i][j] + "), ";

        if ( counter === 980 ) { // arbitrary number, max is 1000
          sql = sql.substring(0, sql.length - 2);
          sql += ";";

          promises.push( client.queryObject( sql ) );

          sql = sqlBase;
          counter = 0;
        }

        counter += 1;
      }
    }

    if ( counter > 0 ) {
      sql = sql.substring(0, sql.length - 2);
      sql += ";";

      promises.push( client.queryObject( sql ) );
    }

    await Promise.all( promises );

    return;

  }
 
  async get(): Promise<Matrix> {
    
    let sql = `SELECT (SELECT COUNT(1) from word) as rows, (SELECT COUNT(1) from concept) as columns;`;
    const shape = (await client.queryObject<{rows: number, columns: number}>(sql)).rows[0];
    
    const outArr: number[][] = [];
    for ( let i = 0; i < shape.rows; i++ ) {
      outArr[i] = [];
      for ( let j = 0; j < shape.columns; j++ ) {
        outArr[i][j] = 0;
      }
    }

    const outMat: Matrix = new Matrix(outArr);

    sql = "SELECT * FROM ubase_vector;";

    const qResult = await client.queryObject<{id: number, concept_id: number, val: number}>(sql);

    for ( let val of qResult.rows) {
      outMat.matrix[val.id - 1][val.concept_id - 1] = val.val;
    }

    return new Matrix( outArr) ; 
  }
}

export default new UBaseRepo();
