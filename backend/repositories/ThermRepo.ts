import { client, Matrix } from "../deps.ts";

class ThermRepo {
  async get(): Promise<Matrix> {
    
    let sql = `SELECT (SELECT COUNT(1) from word) as rows, (SELECT COUNT(1) from doc) as columns;`;
    const shape = (await client.queryObject<{rows: number, columns: number}>(sql)).rows[0];
    
    const outArr: number[][] = [];
    for ( let i = 0; i < shape.rows; i++ ) {
      outArr[i] = [];
      for ( let j = 0; j < shape.columns; j++ ) {
        outArr[i][j] = 0;
      }
    }

    const outMat: Matrix = new Matrix(outArr);

    sql = "SELECT * FROM therm_vector;";

    const qResult = await client.queryObject<{id: number, concept_id: number, val: number}>(sql);

    for ( let val of qResult.rows) {
      outMat.matrix[val.id - 1][val.concept_id - 1] = val.val;
    }

    return new Matrix( outArr ); 
  }
}

export default new ThermRepo();
