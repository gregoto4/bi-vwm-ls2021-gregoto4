import { client, Document } from "../deps.ts";

class DocumentRepo {
  // async get ( words: string[] ) : string {
  //   // client.queryArray<Document>();

  //   return await "";
  // }
  async getCountByWord(wordId: number): Promise<number> {
    const sql: string = "SELECT * FROM term_vector WHERE word_id = '" + wordId +
      "';" as string;
    const count = await client.queryObject(sql);
    return count.rowCount as number;
  }
  async getCount(): Promise<number> {
    const sql: string = "SELECT * FROM doc;" as string;
    const count = await client.queryObject(sql);
    return count.rowCount as number;
  }

  async set(document: string, words: Map<string, number>) {
    let sql = "INSERT INTO doc ( name ) VALUES ('" + document + "' );";
    await client.queryObject(sql);

    sql = "SELECT id FROM doc WHERE name = '" + document + "' ;";
    const dId = await client.queryObject(sql);

    for (const w of words.keys()) {
      const wquery = "SELECT id FROM word WHERE word = '" + w + "';";
      const wId = await client.queryObject(wquery);
      sql = "INSERT INTO term_vector ( id, word_id, count) VALUES ( " +
        dId.rows[0].id + ", '" + wId.rows[0].id + "' ," + words.get(w) + ") ON CONFLICT DO NOTHING;";
      await client.queryObject(sql);
    }

    return;
    // insert document words

    // client.queryObject( sql );
  }
}

export default new DocumentRepo();
