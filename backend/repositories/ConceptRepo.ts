import { client, Matrix } from "../deps.ts";

class ConceptRepo {
  async deleteTableContent(): Promise<void> {
    const sql = "DELETE FROM concept_vector; DELETE FROM concept;";
    await client.queryObject(sql);
    return;
  }
  async set(mat: number[][]): Promise<void> {
    this.deleteTableContent();

    let sql: string = "INSERT INTO concept (id) VALUES ";
    for ( let i = 0; i < mat.length; i++ ) {
      sql += "( " + (i + 1) + " ), ";
    }

    sql = sql.substring(0, sql.length - 2);
    sql += ";";

    await client.queryObject( sql );
    
    const sqlBase = "INSERT INTO concept_vector ( id, doc_id, weight ) VALUES ";
    sql = sqlBase;

    const promises: Promise<any>[] = [];
    let counter = 0;
    for ( let i = 0; i < mat.length; i++ ) {
      for ( let j = 0; j < mat[0].length; j++ ) {
        sql += "( "+ (i + 1) + ", " + (j + 1) + ", " + mat[i][j] + "), ";

        if ( counter === 980 ) { // arbitrary number, max is 1000
          sql = sql.substring(0, sql.length - 2);
          sql += ";";

          promises.push( client.queryObject( sql ) );

          sql = sqlBase;
          counter = 0;
        }

        counter += 1;
      }
    }

    if ( counter > 0 ) {
      sql = sql.substring(0, sql.length - 2);
      sql += ";";

      promises.push( client.queryObject( sql ) );
    }

    await Promise.all( promises );

    return;
  }

  async get(): Promise<Matrix> {
    let sql = `SELECT (SELECT COUNT(1) from concept) as rows, (SELECT COUNT(1) from doc) as columns;`;
    const shape = (await client.queryObject<{rows: number, columns: number}>(sql)).rows[0];
    
    const outArr: number[][] = [];
    for ( let i = 0; i < shape.rows; i++ ) {
      outArr[i] = [];
      for ( let j = 0; j < shape.columns; j++ ) {
        outArr[i][j] = 0;
      }
    }

    const outMat: Matrix = new Matrix(outArr);

    sql = "SELECT * FROM concept_vector;";

    const qResult = await client.queryObject<{id: number, doc_id: number, weight: number}>(sql);

    for ( let val of qResult.rows) {
      outMat.matrix[val.id - 1][val.doc_id - 1] = val.weight;
    }

    return new Matrix( outArr ) ; 
  }
}

export default new ConceptRepo();
