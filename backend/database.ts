import { Client } from "https://deno.land/x/postgres/mod.ts";

const client = new Client({
      database: "searchengine",
      hostname: "postgres",
      password: "secret",
      port: 5432,
      user: "engineuser"
    });

await client.connect();

export { client };