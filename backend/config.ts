export const APP_HOST = "0.0.0.0";
export const APP_PORT = Deno.env.get("APP_PORT") || 8000;