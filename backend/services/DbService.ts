import WordRepo from "../repositories/WordRepo.ts"
import DocumentRepo from "../repositories/DocumentRepo.ts"
import { client, Matrix } from "../deps.ts"
import UBaseRepo from "../repositories/UBaseRepo.ts"
import ConceptRepo from "../repositories/ConceptRepo.ts"


export const findDocumnets = async function (query: string): Promise<string> {

  let sql = `SELECT (SELECT COUNT(1) from word) as rows, (SELECT COUNT(1) from doc) as columns;`;

  let termShape = (await client.queryObject<{rows: number, columns: number}>(sql)).rows[0];
  termShape.rows = Number( termShape.rows );
  termShape.columns = Number( termShape.columns );

  if (termShape.rows === 0 || termShape.columns === 0) {
    return "";
  }

  sql = "SELECT id FROM word ORDER BY id;";
  const words = (await client.queryObject<{id: number}>(sql)).rows;
  let wordMap: number[] = [];
  
  for ( let i = 0; i < words.length; i++ ) {
    wordMap[words[i].id] = i;
  }
  
  const queryVectArr: number[][] = [];
  // fill column vecotor with zeros
  for ( let i = 0; i < termShape.rows; i++ ) {
    queryVectArr[i] = [];
    queryVectArr[i][0] = 0;
  }

  const qArr = JSON.parse(query);
  for (let i = 0; i < qArr.length; i++) {
    sql =   `SELECT DISTINCT
              word_id,
              w.word,
              WORDCOUNT.doccountbyword
            FROM
              term_vector
            JOIN
              (select
                word_id,
                count(word_id) as doccountbyword
              FROM term_vector
              GROUP BY word_id) as WORDCOUNT using(word_id)
            JOIN
              word w ON term_vector.word_id = w.id
            WHERE
              w.word = '` + ( qArr[i].key as string ).toLowerCase() + "';";

    const dResult = await client.queryObject<{word_id :number, word :string, doccountbyword :number}>(sql);

    // Uses idf weighting scheme for query
    if (dResult.rowCount === 1)
      // queryVectArr[wordMap[dResult.rows[0].word_id]][0] = Math.log2(termShape.columns / Number(dResult.rows[0].doccountbyword) * qArr[i].value);
      queryVectArr[wordMap[dResult.rows[0].word_id]][0] = qArr[i].value;
  }

  // column query vector
  const queryVect: Matrix = new Matrix( queryVectArr );

  //Convert from term space to concept space
  console.log( "Waiting for UBase matrix." );
  let uBase = await UBaseRepo.get();
  uBase = uBase.transpose();

  console.log( "Multiplication starts." );
  // query vector in concept base
  const conceptVector: number[] = uBase.times( queryVect ).transpose().matrix[0];
  console.log( "Multiplication ends." );

  console.log( "Waiting for concept-by-document matrix." );
  const documentByconceptMatrix: Matrix = ( await ConceptRepo.get() ).transpose();
  
  console.log( "Computing cosine distances." );
  // cosine distances for all docs
  let resultsArr: { doc_id: number, weight: number }[] = [];
  for ( let i = 0; i < documentByconceptMatrix.matrix.length; i++ ){
    let documentVector = documentByconceptMatrix.matrix[i];

    let totalProduct: number = 0;
    let queryS: number = 0;
    let docS: number = 0;
    for ( let x = 0; x < documentVector.length; x++ ) {
        totalProduct += documentVector[x] * conceptVector[x];

        docS += Math.pow( documentVector[x], 2 );
        queryS += Math.pow( conceptVector[x], 2 );
    }

    const cosSim = totalProduct / ( Math.sqrt( docS ) * Math.sqrt( queryS ) );

    resultsArr.push( {doc_id: i + 1, weight: Math.acos( cosSim )} );
  }

  // sort documnets by weight
  resultsArr = resultsArr.sort( (a, b) => { return a.weight - b.weight; } );

  const queryResult: { document: string, weight: number }[] = [];

  const RESULT_NUM: number = 5;
  let cnt = 0;
  for (let r of resultsArr) {
    sql = "SELECT name FROM doc WHERE id =" + r.doc_id + ";";
    const temp = await client.queryObject<{name: string}>(sql);

    queryResult.push({document: temp.rows[0].name, weight: r.weight });

    if ( ++cnt === RESULT_NUM )
      break;
  }

  return JSON.stringify(queryResult);

};


export const sendWords = async function( docName: string,  document: string[] ): Promise<void> {

  const words: Map<string, number> = new Map();

  for ( const word of document ) {
    if ( words.has( word ) )
      words.set( word, ( words.get( word ) as number )  + 1 );
    else
      words.set( word, 1 );
  }

  await WordRepo.set(words);
  console.log(docName);
  await DocumentRepo.set( docName, words );

  return;

};
