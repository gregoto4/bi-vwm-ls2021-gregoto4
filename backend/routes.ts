import { Router, RouterContext, FormDataFile, helpers } from "./deps.ts";
import getResults from "./controllers/getResults.ts";
import addData from "./controllers/addData.ts";
import calcMatrix from "./controllers/calcMatrix.ts";

const router = new Router();

router
  .get( "/compute", async ( ctx: RouterContext ) => {
    ctx.response.body = await calcMatrix();
  });

router
  .get("/query", async ( ctx: RouterContext ) => {
    ctx.response.body = await getResults( helpers.getQuery( ctx ) );
  });
  
router
  .post( "/data", async ( ctx: RouterContext ) => {

    const body = await ctx.request.body({ type: 'form-data'});
    const formData = await body.value.read();

    ctx.response.body = await addData( formData.files as FormDataFile[] );

  });

export default router;