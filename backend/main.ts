import { Application, oakCors } from "./deps.ts";
import { APP_HOST, APP_PORT } from "./config.ts";
import router from "./routes.ts";

//TODO, maybe???
// import _404 from "./controllers/404.js";
// import errorHandler from "./controllers/errorHandler.js";

const app = new Application();

// disable CORS
app.use(
  oakCors({
    origin: "*"
  }),
);
 
app.use(router.routes());
app.use(router.allowedMethods());

 
app.addEventListener('listen', () => {
  console.log(`Listening on: localhost:${APP_PORT}`);
});
 
await app.listen( `${APP_HOST}:${APP_PORT}` );
